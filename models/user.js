const users = require('../userlist.json');
exports.get = function(id) {
    return id ? users.find(user => user._id === id) : users;
};
exports.create = function(user) {
    users.push(user);
};
exports.update = function(id, updateUser) {
    let user = exports.get(id);
    let status = false;
    if (user) {
         Object.assign(user, updateUser);
         status = true;
    }
    return status;
};
exports.delete = function(id) {
    
    for(let i = users.length - 1; i > -1; i--) {
        if(users[i]._id === id) {
            users.splice(i, 1);
            return true;
        }
    }
    return false;
       
}
