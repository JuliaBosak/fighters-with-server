import FightersView from './fightersView';
import { fighterService } from './services/fightersService';
import GameView from './gameView';
import { constants } from 'zlib';
class App {
  constructor() {
    this.startApp();
  }

  static rootElement = document.getElementById('root');
  static loadingElement = document.getElementById('loading-overlay');

  async startApp() {
    try {
      App.loadingElement.style.visibility = 'visible';
      
      const fighters = await fighterService.getFighters();
      const fightersView = new FightersView(fighters);
      const fightersElement = fightersView.element;
      App.rootElement.appendChild(fightersElement);
      const popupResult = document.getElementById('popup-result');
      const message = document.getElementById('message');
      const buttonStart = document.getElementById('start-button');
      const buttonClose =  popupResult.querySelector('.popup-content').querySelector('.close');
  
    
      buttonStart.addEventListener('click', () => {
        const popupGame = document.getElementById('popup-game');
        const fighters = document.querySelector('.fighters');
        const checkedFighters = fighters.querySelectorAll('.checkbox:checked');
        const fightersDetailsMap = fightersView.fightersDetailsMap;
        if(checkedFighters.length === 2) {
          if(!fightersDetailsMap.has(checkedFighters[0].id) || !fightersDetailsMap.has(checkedFighters[1].id)) {
            popupResult.style.display = 'flex';
            message.innerText = 'Спочатку перегляньте інформацію про бійця';
            return false;
          }
          const startGame = new GameView(fightersDetailsMap.get(checkedFighters[0].id), fightersDetailsMap.get(checkedFighters[1].id));
         
      }
       else {
        popupResult.style.display = 'flex';
        message.innerText = 'Ви обрали невідну кількість бійців, оберіть двох.';
      }
      });
      buttonClose.addEventListener('click', () => {
        popupResult.style.display = 'none';
      });
    } catch (error) {
      console.warn(error);
      App.rootElement.innerText = 'Failed to load data';
    } finally {
      App.loadingElement.style.visibility = 'hidden';
    }
  }
}

export default App;