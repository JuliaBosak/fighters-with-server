export default class Fighter {
    constructor(name, attack, health, defense) {
       this.name = name;
       this.attack = attack;
       this.health = health;
       this.defense = defense;
    }
    getHitPower() {
        const
            criticalHitChance =  Math.random()  + 1,
            power = this.attack * criticalHitChance;
            return power;     
    } 

    getBlockPower() {
        const
            dodgeChance = Math.random()  + 1,
            block = this.defense * dodgeChance;
            return block;
    } 
}
