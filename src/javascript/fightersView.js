import View from './view';
import FighterView from './fighterView';
import { fighterService } from './services/fightersService';

class FightersView extends View {
  constructor(fighters) {
    super();
    
    this.handleClick = this.handleFighterClick.bind(this);
    this.handleMouseLeave = this.handleFighterLeave.bind(this);
    this.createFighters(fighters);
  }

   fightersDetailsMap = new Map();

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick, this.handleMouseLeave, true);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters'});
    this.element.append(...fighterElements);
  }

  async setFighterDetailsInMap(fighter) {
    const fighterDetails = await fighterService.getFighterDetails(fighter._id);
    this.fightersDetailsMap.set(fighter._id, fighterDetails);
    return fighterDetails;
  }

  createModalFighterInfo(fighterDetails) {
    this.element = this.createElement({ tagName: 'form', className: 'info-popup' });
    const  details = [];
    Object.keys(fighterDetails).forEach( (key)=> {
     switch(key) {
      case 'attack':
      case 'health':
      case 'defense':
          const 
          detail = this.createElement({tagName: 'div', className: 'fighter-detail'}),
          detailName = this.createElement({tagName: 'span', className: 'fighter-detail-name'}),
          detailValue = (key === 'defense') ? this.createElement({ tagName: 'input', className: 'fighter-detail-value', attributes: {readonly: ''}}) : 
          this.createElement({ tagName: 'input', className: 'fighter-detail-value'});
          detailValue.value = fighterDetails[key];
          detailValue.addEventListener('change', () =>  {
            fighterDetails[key] = +detailValue.value;
            fighterService.updateFihghterDetails(fighterDetails);
            const changed = this.createElement({ tagName: 'span', className: 'fighter-detail-change' });
            changed.innerText = 'Changed!';
            this.element.insertBefore(changed, this.element.firstChild);
          });
          detailName.innerText = key;
          detail.append(detailName, detailValue);
          details.push(detail);
     }
     
   });
    this.element.append(...details);
    return this.element;
  }

  handleFighterClick(event, fighter, fighterElement) {
    const fighterWrap = fighterElement.querySelector('.fighter-wrap');
    if(!fighterWrap.querySelector('.info-popup')) {
    if(!this.fightersDetailsMap.has(fighter._id)) {
      try {
      this.setFighterDetailsInMap(fighter)
      .then(
        response => {
            const modal = this.createModalFighterInfo(response);
            fighterWrap.append(modal);
      },
        error => {
          throw error;
      });
      }
      catch(error) {
        throw error;
      }
    }
    else {
      const modal = this.createModalFighterInfo(this.fightersDetailsMap.get(fighter._id));
      fighterWrap.append(modal);
    }
  }
  }

  handleFighterLeave(event, fighter, fighterElement) { 
    const fihterWrap = fighterElement.querySelector('.fighter-wrap');
    if(fihterWrap.querySelector('.info-popup')) {
    fihterWrap.removeChild(fihterWrap.querySelector('.info-popup'));
    }
  } 
}

 

export default FightersView;