import View from './view';

class FighterView extends View {
  constructor(fighter, handleClick, handleMouseLeave,withCheckbox) {
    super();

    this.createFighter(fighter, handleClick, handleMouseLeave, withCheckbox);
  }

  createFighter(fighter, handleClick = () => {}, handleMouseLeave = () => {}, withCheckbox = false) {
    const { name, source } = fighter;
    const nameElement = this.createName(name);
    const imageElement = this.createImage(source);
    const fighterElement = this.createElement({ tagName: 'div', className: 'fighter-wrap'});
    this.element =  this.createElement({ tagName: 'div', className: 'fighter' });
    fighterElement.append(imageElement, nameElement);
    this.element.append(fighterElement);

    if(withCheckbox) {
      const checkboxElement = this.createDivWithCheckbox(fighter._id);
      this.element.append(checkboxElement);
      fighterElement.addEventListener('click', event => handleClick(event, fighter, this.element));
      fighterElement.addEventListener('mouseleave', event => handleMouseLeave(event, fighter, this.element));
    }
    else {
      this.element.append(this.createDescription(fighter));

    }
    
  }

  createName(name) {
    const nameElement = this.createElement({ tagName: 'span', className: 'name' });
    nameElement.innerText = name;

    return nameElement;
  }

  createImage(source) {
    const attributes = { src: source };
    const imgElement = this.createElement({
      tagName: 'img',
      className: 'fighter-image', 
      attributes
    });

    return imgElement;
  }
  createDivWithCheckbox(id) {
    const div = this.createElement({tagName: 'div', className: 'wrapper-checkbox'});
    const checkbox = this.createElement({tagName: 'input', className: 'checkbox', attributes: {type: 'checkbox', id: id}});
    const label = this.createElement({tagName: 'label', className: 'label', attributes: {for: id}});
    label.innerText = 'Вибрати';
    div.append(checkbox, label);
    return div;
  }
  createDescription(fighter) {
    const wrap = this.createElement({ tagName: 'div', className: 'fighter-details' });
    const attackDescription = this.createItem('attack', fighter.attack).wrap;
    const healthItem = this.createItem('health', fighter.health);
    const defenseDescription = this.createItem('defense', fighter.defense).wrap;
    this.health = healthItem.value;
    wrap.append(attackDescription, healthItem.wrap, defenseDescription);
    return wrap;

     
 }
  createItem(name, value) {
      const wrap = this.createElement({ tagName: 'div', className: 'fighter-detail-wrap' });
      const spanForName = this.createElement({ tagName: 'span', className: 'fighter-detail-item'});
      const spanForValue = this.createElement({ tagName: 'span', className:  `${name}` });
      spanForName.innerText = name;
      spanForValue.innerText = value;
      wrap.append(spanForName, spanForValue);
      return  {wrap, value: spanForValue};
 }
 updateHealth(health) {
  this.health.innerText = health;
 }

}

export default FighterView;