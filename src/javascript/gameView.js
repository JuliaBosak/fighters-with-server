import FighterView from './fighterView';
import Fighter from './fighter';
import View from './view';

class GameView extends View {
    constructor(firstFighterDetails, secondFighterDetails) {
        super();
        this.firstFighterDetails = firstFighterDetails;
        this.secondFighterDetails = secondFighterDetails;
        this.viewGamePopup();
        this.fight();
    }
    popupGame;
     viewGamePopup() {
        this.popupGame = document.getElementById('popup-game');
        const fighters = this.popupGame.querySelector('.fighters');
        if(fighters.getElementsByClassName('fighter').length !== 0) {
            fighters.innerText = '';
         }
            this.firstFighterView = new FighterView(this.firstFighterDetails);
            this.secondFighterView = new FighterView( this.secondFighterDetails);
            this.secondFighterView.element.querySelector('.fighter-image').style.transform = 'scale(-1, 1)';

            fighters.append(this.firstFighterView.element, this.secondFighterView.element);
            
        
        this.popupGame.style.display = 'flex';

        
    }

    fight() {
        const startButton = document.getElementById('fight-start-button');
        const closeButton =  this.popupGame.querySelector('.popup-content').querySelector('.close');            
         const listenerStartButton = () => {
            startButton.disabled = true;
            document.getElementById('message').innerHTML = '';
            this.hit(this.firstFighterDetails, this.secondFighterDetails)
        }
        startButton.addEventListener('click', listenerStartButton);
        
        closeButton.addEventListener('click', () => {
            startButton.removeEventListener('click', listenerStartButton);
            clearInterval(this.fightInterval);
            startButton.disabled = false;
            this.popupGame.style.display = 'none';
        });   
    }
    kick(firstFighter, secondFighter, fighterView) {
        if (secondFighter.health > 0 ) {
        const damage = firstFighter.getHitPower() - secondFighter.getBlockPower()
        secondFighter.health -= damage > 0  
                        ? damage
                        : 0   
            if(secondFighter.health > 0) {
                fighterView.updateHealth(Math.round(secondFighter.health));
            }                     
        }
    }

    hit(first, second) {
        const firstFighterForGame = new Fighter(first.name, first.attack, first.health, first.defense);
        const secondFighterForGame = new Fighter(second.name, second.attack, second.health, second.defense);
        const popupResult = document.getElementById('popup-result');
            this.fightInterval = setInterval( () => {
                if(firstFighterForGame.health > 0 && secondFighterForGame.health > 0) {
                    this.kick(firstFighterForGame,secondFighterForGame, this.secondFighterView);
                    this.kick(secondFighterForGame,firstFighterForGame, this.firstFighterView);
                }
                else {
                    clearInterval(this.fightInterval);
                     const message = document.getElementById('message');
                     message.innerText = `Переміг ${(firstFighterForGame.health > secondFighterForGame.health) ? 
                        firstFighterForGame.name :
                        secondFighterForGame.name}`;
                    popupResult.style.display = 'flex';

                } 
             }, 500);
}   

}
export default GameView;