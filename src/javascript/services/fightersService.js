import { callApi } from '../helpers/apiHelper';

class FighterService {
  async getFighters() {
    
    return await callApi('/user', { method: 'GET'});
  }

  async getFighterDetails(_id) {
    
    return await callApi(`/user/${_id}`, { method: 'GET'});
  }
  async updateFihghterDetails(body) {
    return await  callApi(`/user/${body._id}`, 
    {
      method: 'PUT',
      header: {  
        "Content-type": "application/json"  
      },
      body: `${body}`
    });
  }
}

export const fighterService = new FighterService();
