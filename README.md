#### How to start the code

1. `git clone https://bitbucket.org/JuliaBosak/fighters-with-server.git`
2. `cd fighters-with-server`
3. `npm i`
4. `npm start`
5. By default server running on [localhost:3000](http://localhost:3000)

### Requests:

- **GET**: _/user_  
  get an array of all users

- **GET**: _/user/:id_  
  get one user by ID

- **POST**: _/user_  
  create user according to the data from the request body

- **PUT**: _/user/:id_  
  update user according to the data from the request body

- **DELETE**: _/user/:id_  
  delete one user by ID