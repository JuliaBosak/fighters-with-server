const { body } = require('express-validator/check');
const users = require('../models/user');

const checkFighterParams = function(optional = false) {

    return optional ? [
        body('_id')
            .optional()
            .isString()
            .isLength({ min: 1 }),
        body('name')
            .optional()
            .isString()
            .isLength({ min: 2 }),
        body('attack', 'defense', 'health')
            .optional()
            .isInt({min: 1}),
        body('source')
            .optional()
            .isURL({ protocols: ['http','https']})
    ] :
    [
        body('name')
            .isString()
            .isLength({ min: 2 }) ,
        body('attack', 'defense', 'health')
            .isInt({min: 1}),
        body('source')
            .isURL({ protocols: ['http','https']})
    ];

}

const checkFighterId = function(existId = false) {
    const checkId = body('_id').isString().isLength({ min: 1 });
    return existId ? checkId.custom( value => !!users.get(value)): checkId.custom( value => !users.get(value));

}
module.exports = {
    checkFighterParams,
    checkFighterId
}

