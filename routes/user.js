var express = require('express');
var router = express.Router();
const {validationResult } = require('express-validator/check');
const {checkFighterParams, checkFighterId } = require('../middlewares/validation.middleware');
const users = require('../models/user');
const existId = true;
const optional = true;

router
  .route('/')
  .get((req, res) => {
    res.json(users.get());
  })
  .post( 
    [checkFighterId(), ...checkFighterParams()],
    (req, res) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
      }

      res.json(users.create(req.body));
  })


router
  .route('/:id')
  .get((req, res) => {
    const user = users.get(req.params.id);
    if (user) {
      res.json(user);
    }
    else {
      res.sendStatus(404);
    }
  })
  .put(
    
    [...checkFighterParams(optional)],
    (req, res) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
      }

      res.json(users.update(req.params.id, req.body));
  })
  .delete (
    (req, res) => {
      res.json(users.delete(req.params.id));
})



module.exports = router;
